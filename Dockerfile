from tclavier/nginx

add cloud.azae.net.conf  /etc/nginx/sites-enabled/
add helium.azae.net.conf /etc/nginx/sites-enabled/
add mobilizon.azae.net.conf /etc/nginx/sites-enabled/
add office.azae.net.conf /etc/nginx/sites-enabled/
add www.azae.net.conf    /etc/nginx/sites-enabled/
