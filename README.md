Configuration file to deploy on https://deliverous.com

nextcloud migrate : 

    docker exec -it --user www-data a384e743-0a1b-453e-bfa5-e01802922252.Aza.nextcloud.service bash
    ./occ db:convert-type --password $POSTGRES_PASSWORD --all-apps --clear-schema pgsql $POSTGRES_USER $POSTGRES_HOST $POSTGRES_DB
