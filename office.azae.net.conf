server {
  listen       80;
  server_name  office.azae.net;
  return       301 https://office.azae.net$request_uri;
}


server {
    listen       443 ssl;
    server_name  office.azae.net;

    ssl_certificate     /etc/letsencrypt/live/office.azae.net/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/office.azae.net/privkey.pem;

    # static files
    location ^~ /loleaflet {
        proxy_pass https://collabora:9980;
        proxy_set_header Host $http_host;
    }

    # WOPI discovery URL
    location ^~ /hosting/discovery {
        proxy_pass https://collabora:9980;
        proxy_set_header Host $http_host;
    }

    # main websocket
    location ~ ^/lool/(.*)/ws$ {
        proxy_pass https://collabora:9980;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "Upgrade";
        proxy_set_header Host $http_host;
        proxy_read_timeout 36000s;
    }

    # download, presentation and image upload
    location ~ ^/lool {
        proxy_pass https://collabora:9980;
        proxy_set_header Host $http_host;
    }

    # Admin Console websocket
    location ^~ /lool/adminws {
        proxy_pass https://collabora:9980;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "Upgrade";
        proxy_set_header Host $http_host;
        proxy_read_timeout 36000s;
    }

    # Capabilities
    location ^~ /hosting/capabilities {
        proxy_pass https://collabora:9980;
        proxy_set_header Host $http_host;
    }
}
